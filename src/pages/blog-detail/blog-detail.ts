import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';
import {Observable} from "rxjs";
import {Blog, BlogProvider} from "../../providers/blog/blog";
import {Comment, CommentProvider} from "../../providers/comment/comment";
import {tap} from "rxjs/operators";
import {BlogListPage} from "../blog-list/blog-list";

/**
 * Generated class for the BlogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blog-detail.html',
})
export class BlogDetailPage {
  public pk: number | string;

  public blog$: Observable<Blog>;
  public comments$: Observable<Comment[]>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private blogData: BlogProvider,
              private alertCtrl: AlertController,
              private commentData: CommentProvider) {
  }

  presentPromptComment() {
  let alert = this.alertCtrl.create({
    title: 'Comment',
    inputs: [
       {
        name: 'blog',
        value:this.navParams.get('pk'),
        type: 'hidden'
      },
      {
        name: 'body',
        placeholder: 'Inhalt',
        type: 'text'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Add',
        handler: data => {
          this.commentData.create(data).subscribe(result => {
            console.log('Comment created', result);
            this.comments$ = this.commentData.getCommentByFilter('blog='+this.pk);
          })

        }
      }
    ]
  });
  alert.present();
}
 editBlog(pk: number | string,title:string,body:string,image:string) {
    console.log(pk);
    let alert = this.alertCtrl.create({
    title: 'Blog',
    inputs: [
       {
        name: 'id',
        value:this.navParams.get('pk'),
        type: 'hidden'
      },
      {
        name: 'title',
        value: title,
        type: 'text'
      },
      {
        name: 'image',
        value: image,
        type: 'text'
      },
      {
        name: 'body',
        value: body,
        type: 'text'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Edit',
        handler: data => {
          this.blogData.update(data).subscribe(result => {
            console.log('Blog edited', result);
            this.ionViewDidLoad();

          })

        }
      }
    ]
  });
  alert.present();
}

  public removeBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Datensatz ' + pk + ' wirklich löschen?',
      buttons: ['Abbrechen', {
        text: 'Yes',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.navCtrl.push(BlogListPage)
          })
        }
      }]
    });
    alert.present( );


  }


  private loadComments() {

    this.comments$ = this.commentData.getComments.pipe(
      tap(data => console.log(data))
    );
  }


  ionViewDidLoad() {
    this.pk = this.navParams.get('pk');
    this.blog$ = this.blogData.getBlog(this.pk);
    this.loadComments();
    this.comments$ = this.commentData.getCommentByFilter('blog='+this.pk);
    console.log('ionViewDidLoad BlogDetailPage');
  }
}
