import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Blog, BlogProvider} from "../../providers/blog/blog";
import {Observable} from "rxjs";
import {BlogDetailPage} from "../blog-detail/blog-detail";
import {tap} from "rxjs/operators";

/**
 * Generated class for the BlogListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-list',
  templateUrl: 'blog-list.html',
})
export class BlogListPage {
  public blogs$: Observable<Blog[]>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private blogData: BlogProvider) {
  }


presentPromptBlog() {
  let alert = this.alertCtrl.create({
    title: 'Blog',
    inputs: [
      {
        name: 'title',
        placeholder: 'Blog Titel'
      },
      {
        name: 'body',
        placeholder: 'Inhalt',
        type: 'text'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log(data +'Cancel clicked');
        }
      },
      {
        text: 'Add',
        handler: data => {
          this.blogData.create(data).subscribe(result => {
            console.log('Blog created', result);
            this.loadBlogs();
          })

        }
      }
    ]
  });
  alert.present();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogListPage');

    this.loadBlogs();
  }

  private loadBlogs() {

    this.blogs$ = this.blogData.getBlogs.pipe(
      tap(data => console.log(data))
    );
  }

  public goToDetail(pk: number | string) {
    this.navCtrl.push(BlogDetailPage, {pk: pk});
  }

  public removeBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Datensatz ' + pk + ' wirklich löschen?',
      buttons: ['Dismiss', {
        text: 'Yes',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.loadBlogs();
          })
        }
      }]
    });
    alert.present( );


  }
}
