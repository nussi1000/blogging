import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {BlogListPage} from "../pages/blog-list/blog-list";
import {BlogDetailPage} from "../pages/blog-detail/blog-detail";
import { BlogProvider } from '../providers/blog/blog';
import {HttpClientModule} from "@angular/common/http";
import { CommentProvider } from '../providers/comment/comment';

// Rest API Server URL
export const BASE_URL = 'https://blogs.ymjproductions.ch/api/v1';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    BlogListPage,
    BlogDetailPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    BlogListPage,
    BlogDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BlogProvider,
    CommentProvider
  ]
})
export class AppModule {}
